package property;

public class Laptop extends Property {
    private String characteristics;

    public Laptop(String color, String vendor, int price) {
            super(color, vendor, price);
    }

    public Laptop(String color, String vendor, int price, String characteristics) {
        this(color, vendor, price);
        this.characteristics = characteristics;
    }
    public String getInfo(){
        return toString();
    }

    @Override
    public String toString() {
        return "Laptop{" +
                "characteristics='" + characteristics + '\'' +
                ", color='" + color + '\'' +
                ", vendor='" + vendor + '\'' +
                ", price=" + price +
                '}';
    }
}
