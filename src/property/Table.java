package property;

public class Table extends Property {
    private String material;
    private int size;

    public Table(String color, String vendor, int price) {
        super(color, vendor, price);
    }

    public Table(String color, String vendor, int price, String material, int size) {
        this(color, vendor, price);
        this.material = material;
        this.size = size;
    }
    public String getInfo(){
        return toString();
    }

    @Override
    public String toString() {
        return "Table{" +
                "material='" + material + '\'' +
                ", size=" + size +
                ", color='" + color + '\'' +
                ", vendor='" + vendor + '\'' +
                ", price=" + price +
                '}';
    }
}
