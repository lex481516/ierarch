package property;

public class Property {
    protected String color;
    protected String vendor;
    protected int price;

    public Property(){}

    public static void main(String[] args){
        (new Property()).runApp();
    }

    public void runApp(){
        Property[] prop = new Property[2];
        Car car = new Car("blue","BMW",100500,300,"auto","aga","A");
        prop[0] =car;
        Fridge fridge = new Fridge("Silver","Horizont",100, 2, 4, 5);
        prop[1] = fridge;
        for (int i=0;i<prop.length;i++){
            System.out.println("тип "+prop[i].getInfo());
        }
    }

    public Property(String color, String vendor, int price) {
        this.color = color;
        this.vendor = vendor;
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    public String getInfo(){
        return "info";
    }
}
