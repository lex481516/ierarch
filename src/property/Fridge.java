package property;

public class Fridge extends Property {
    private int cameraCount;
    private int volume;
    private int energyConsClace;

    public Fridge(String color, String vendor, int price) {
        super(color, vendor, price);
    }

    public Fridge(String color, String vendor, int price, int cameraCount, int volume, int energyConsClace) {
        this(color, vendor, price);
        this.cameraCount = cameraCount;
        this.volume = volume;
        this.energyConsClace = energyConsClace;
    }
    public String getInfo(){
        return toString();
    }

    @Override
    public String toString() {
        return "Fridge{" +
                "cameraCount=" + cameraCount +
                ", volume=" + volume +
                ", energyConsClace=" + energyConsClace +
                ", color='" + color + '\'' +
                ", vendor='" + vendor + '\'' +
                ", price=" + price +
                '}';
    }
}
