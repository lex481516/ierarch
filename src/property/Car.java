package property;

public class Car extends Property {
    private int maxSpeed;
    private String transmission;
    private String vechiclevol;
    private String tankType;

    public Car(String color, String vendor, int price) {
        super(color, vendor, price);
    }

    public Car(String color, String vendor, int price, int maxSpeed, String transmission, String vechiclevol, String tankType) {
        this(color, vendor, price);
        this.maxSpeed = maxSpeed;
        this.transmission = transmission;
        this.vechiclevol = vechiclevol;
        this.tankType = tankType;
    }
    public String getInfo(){
        return toString();
    }

    @Override
    public String toString() {
        return "Car{" +
                "maxSpeed=" + maxSpeed +
                ", transmission='" + transmission + '\'' +
                ", vechiclevol='" + vechiclevol + '\'' +
                ", tankType='" + tankType + '\'' +
                ", color='" + color + '\'' +
                ", vendor='" + vendor + '\'' +
                ", price=" + price +
                '}';
    }
}
